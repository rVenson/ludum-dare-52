extends KinematicBody

const PlayerController = preload("res://src/player/PlayerController.gd")

onready var navigation_agent = $NavigationAgent
var total_life = 2
var current_life = 1
var speed = 5
var damage = 1
var default_attack_delay = 1.5
var current_attack_delay = 1.5
var raid_target = null
var target = null
var attacking = null

func _physics_process(delta):
	current_attack_delay -= delta
	if (attacking):
		if (current_attack_delay < 0):
			attack()
			current_attack_delay = default_attack_delay
		return
	if (raid_target):
		move_to_target(raid_target, delta)
		return
	if (target):
		move_to_target(target, delta)

func set_target(target : Spatial):
	self.target = target

func set_raid_target(raid_target):
	self.raid_target = raid_target

func move_to_target(target, delta):
	var current_position = self.global_transform.origin
	var target_position = target.global_transform.origin
	var distance = current_position.distance_squared_to(target_position)
	if (distance < 1): return
	navigation_agent.set_target_location(target_position)
	var next_position = navigation_agent.get_next_location()
	var direction = current_position.direction_to(target_position)
	look_at(next_position, Vector3.UP)
	move_and_collide(direction * delta * speed)

func damage(value = 1):
	current_life -= 1
	if (current_life <= 0):
		death()

func death():
	queue_free()

func attack():
	self.attacking.damage()

func _on_TargetArea_body_entered(body : Spatial):
	if (body is PlayerController):
		target = body

func _on_TargetArea_body_exited(body : Spatial):
	if (body is PlayerController):
		target = null

func _on_AttackArea_body_entered(body : Spatial):
	if (body.is_in_group("damageable")):
		attacking = body

func _on_AttackArea_body_exited(body : Spatial):
	if (body == attacking):
		attacking = null

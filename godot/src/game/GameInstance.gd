extends Node

const CropModel = preload("res://src/core/models/CropModel.gd")
const CompoundModel = preload("res://src/core/models/CompoundModel.gd")
const Inventory = preload("res://src/player/Inventory.gd")
const PlayerController = preload("res://src/player/PlayerController.gd")
const ActorModel = preload("res://src/core/models/ActorModel.gd")

onready var world = $World
onready var ui = $UI
onready var game_director = $GameDirector
var unlocked_seeds = Array()
var unlocked_buildings = Array()

func _ready():
	ServiceProvider.Game = self
	ServiceProvider.UIManager = ui
	start_game()
	get_tree().paused = false

func _unhandled_key_input(event):
	if event.is_action_pressed("ui_cancel"):
		get_tree().set_input_as_handled()
		ui.pause_menu()

func unlock_building(building_name):
	var building = ServiceProvider.GameDatabase.buildings.get(building_name)
	if (!building):
		ServiceProvider.Logging.error("Impossible to unlock this building %s" % building_name)
		return
	unlocked_buildings.append(building)

func start_game():
	ServiceProvider.Logging.info("Game started")
	game_director.start(self)

func win_game():
	set_process_unhandled_key_input(false)
	ServiceProvider.Logging.info("Starting end sequence")
	get_tree().paused = true
	ui.show_close_layer()
	yield(get_tree().create_timer(6), 'timeout')
	get_tree().change_scene("res://src/scene/end_game/EndGame.tscn")

func game_over():
	set_process_unhandled_key_input(false)
	ServiceProvider.Logging.info("Starting game over sequence")
	get_tree().paused = true
	ui.show_close_layer()
	yield(get_tree().create_timer(6), 'timeout')
	ui.game_over()

func dialogue(message, actor_name = "unknown"):
	var actor : ActorModel = ServiceProvider.GameDatabase.actors.get(
		actor_name,
		ActorModel.new()
	)
	ui.dialogue_layer.add_message(
		message,
		actor
	)

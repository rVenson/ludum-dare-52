extends Node

const GameInstance = preload("res://src/game/GameInstance.gd")
const DialogueLayer = preload("res://src/ui/layers/dialogue_layer/DialogueLayer.gd")
const CompoundModel = preload("res://src/core/models/CompoundModel.gd")

const DEFAULT_MESSAGE_DELAY = 10.0
var game : GameInstance = null
var dialogue : DialogueLayer = null
var actors = null
var world = null
var milestones = []

func start(game_instance):
	game = game_instance
	dialogue = game.ui.dialogue_layer
	actors = ServiceProvider.GameDatabase.actors
	world = game.world
	populate_world()
	yield(get_tree().create_timer(3, false), "timeout")
	first_message()

func populate_world():
	var metallic_compound = ServiceProvider.GameDatabase.compounds.get('metallic_compound')
	var organic_compound = ServiceProvider.GameDatabase.compounds.get('organic_compound')
	var thermal_compound = ServiceProvider.GameDatabase.compounds.get('thermal_compound')
	var gases_compound = ServiceProvider.GameDatabase.compounds.get('gases_compound')
	world.landed_ship.inventory.add(organic_compound)
	world.landed_ship.inventory.add(organic_compound)
	world.landed_ship.inventory.add(metallic_compound)
	world.landed_ship.inventory.add(metallic_compound)
	world.landed_ship.inventory.add(metallic_compound)
	world.landed_ship.inventory.add(thermal_compound)
	world.landed_ship.inventory.add(thermal_compound)
	world.landed_ship.inventory.add(gases_compound)

### MILESTONES ###

func first_message():
	var milestone_name = "first_message"
	if (milestones.has(milestone_name)) : return
	milestones.append(milestone_name)
	yield(get_tree().create_timer(3 , false), "timeout")
	dialogue.add_message(
		"Hello, World!",
		actors.liekedeeler
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"Your catchphrases are only as good as your ability to calculate the distance between two planets.",
		actors.sarcastic_ai
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"You know we're in this together, don't you?",
		actors.liekedeeler
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"Just until I find some intergalactic connection point. Maybe I can get the hell out of some old wifi spot.",
		actors.sarcastic_ai
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"Okay. My scanners didn't find anything. This planet is basically a big rock of green moss.",
		actors.sarcastic_ai
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"We are screwed?",
		actors.liekedeeler
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"Not totally. We'll need to collect some samples of the local flora to see if we can analyze it.",
		actors.sarcastic_ai
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"We have material for a Biological Analyzer on the ship. Unpack and let's get started.",
		actors.sarcastic_ai
	)
	game.unlock_building('biological_analyser')

func analyser_built():
	var milestone_name = "analyser_built"
	if (milestones.has(milestone_name)) : return
	milestones.append(milestone_name)
	yield(get_tree().create_timer(3, false), "timeout")
	dialogue.add_message(
		"OK. Now find some plants. I'm sure we can extract some fuel from the local flora. Or at least some basic compounds.",
		actors.sarcastic_ai
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"Hey, I'm still the captain here!",
		actors.liekedeeler
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"Ahoy! Captain!",
		actors.sarcastic_ai
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")

func first_seed_researched():
	var milestone_name = "first_seed_researched"
	if (milestones.has(milestone_name)) : return
	milestones.append(milestone_name)
	yield(get_tree().create_timer(3, false), "timeout")
	dialogue.add_message(
		"If we don't start a large-scale production, we'll have to apply for a visa to stay here for a long time.",
		actors.sarcastic_ai
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"Maybe you could, I don't know, start some basic farms.",
		actors.sarcastic_ai
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"Yeap... it's always me who has to do all the hard work!",
		actors.liekedeeler
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"It's because humans are a far superior race, sir. You would hate to be an AI. Immortal and intelligent. What a waste!",
		actors.sarcastic_ai
	)
	game.unlock_building('cultivator')

func first_cultivation():
	var milestone_name = "first_cultivation"
	if (milestones.has(milestone_name)) : return
	milestones.append(milestone_name)
	yield(get_tree().create_timer(3, false), "timeout")
	dialogue.add_message(
		"While waiting for the crops to grow, it's a good idea to look for other plants. Unless you think we're going to leave this planet with this wilted flower.",
		actors.sarcastic_ai
	)
	dialogue.add_message(
		"You should appreciate the nature a little more. Maybe you weren't so grouchy.",
		actors.liekedeeler
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")

func first_harvest():
	var milestone_name = "first_harvest"
	if (milestones.has(milestone_name)) : return
	milestones.append(milestone_name)
	yield(get_tree().create_timer(3, false), "timeout")
	dialogue.add_message(
		"Now use a synthesizer to extract compounds from this plant.",
		actors.sarcastic_ai
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"If you don't have enough compounds in the ship to build one, wander around and I'm sure you'll find something worthwhile in the middle of this ~beautiful planet~.",
		actors.sarcastic_ai
	)
	game.unlock_building('resource_synthesizer')

func enemy_alarm():
	var milestone_name = "enemy_alarm"
	if (milestones.has(milestone_name)) : return
	milestones.append(milestone_name)
	dialogue.add_message(
		"Liekedeeler! There's something strange coming towards the ship! Please hurry!",
		actors.sarcastic_ai
	)

func raid_finished():
	var milestone_name = "raid_finished"
	if (milestones.has(milestone_name)) : return
	milestones.append(milestone_name)
	yield(get_tree().create_timer(3, false), "timeout")
	dialogue.add_message(
		"That was close. Where are these creatures coming from? We need some defense here.",
		actors.liekedeeler
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"For the Turing's sake! I told you to go so far? They almost fried all my circuits!",
		actors.sarcastic_ai
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"Maybe you'll find some interesting military blueprint among the pile of junk you carry on this ship.",
		actors.sarcastic_ai
	)
	game.unlock_building('laser_gunner')

func first_defense():
	var milestone_name = "first_defense"
	if (milestones.has(milestone_name)) : return
	milestones.append(milestone_name)
	yield(get_tree().create_timer(3, false), "timeout")
	dialogue.add_message(
		"This should work!",
		actors.liekedeeler
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"Don't think they will stop. I'm managing to scan signals from more creatures. If I were you, I would prepare better.",
		actors.sarcastic_ai
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"If you weren't on that ship all day, maybe it would help.",
		actors.liekedeeler
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"Who's the grouch now?",
		actors.sarcastic_ai
	)

func last_seed():
	var milestone_name = "last_seed"
	if (milestones.has(milestone_name)) : return
	milestones.append(milestone_name)
	yield(get_tree().create_timer(3, false), "timeout")
	dialogue.add_message(
		"Okay. I think we have everything we need to start mixing the fuel, right?",
		actors.liekedeeler
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"It didn't take long. Use Blender to mix. Don't forget that we're going to need a good amount to get out of this planet's gravity.",
		actors.sarcastic_ai
	)
	game.unlock_building('fuel_blender')

func first_fuel_galoon():
	var milestone_name = "first_fuel_galoon"
	if (milestones.has(milestone_name)) : return
	milestones.append(milestone_name)
	yield(get_tree().create_timer(3, false), "timeout")
	dialogue.add_message(
		"If you are thirsty, you can drink. It is a mixture rich in minerals.",
		actors.sarcastic_ai
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"I'm kidding! I'm not sure you wouldn't try something stupid like that.",
		actors.sarcastic_ai
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"When we get home I'll put you in charge of the refrigerator.",
		actors.liekedeeler
	)
	yield(get_tree().create_timer(DEFAULT_MESSAGE_DELAY, false), "timeout")
	dialogue.add_message(
		"Stay cool, buddy!",
		actors.sarcastic_ai
	)

extends Reference

var store_capacity = 6
var slots = Array()

func set_store_capacity(value):
	self.store_capacity = value

func add(item):
	if (slots.size() >= store_capacity):
		ServiceProvider.Logging.info('Impossible to add %s. Inventory is full.' % item.name)
		return false
	slots.append(item)
	return true

func discard(item):
	var index_to_remove = slots.find(item)
	if (index_to_remove < 0):
		ServiceProvider.Logging.warn('Impossible to remove %s. Item inexistent in the inventory.')
		return false
	slots.remove(index_to_remove)
	return true

func get_all():
	return slots.duplicate()

func is_full(increment_item_list = null, projected_discard = 0):
	var inventory_total = slots.size() - projected_discard
	if (increment_item_list):
		var total_items = 0
		for item_name in increment_item_list:
			var item_total = increment_item_list[item_name]
			total_items += item_total
		inventory_total += total_items
	return inventory_total > store_capacity

func get_resource_map():
	var resource_map = Dictionary()
	var item_list = slots.duplicate()
	for item in item_list:
		if (!resource_map.has(item.name)):
			resource_map[item.name] = 0
		resource_map[item.name] += 1
	return resource_map

func has_resources(required_resource_list : Dictionary):
	var resource_map = get_resource_map()
	for required_resource_name in required_resource_list:
		var required_quantity = required_resource_list[required_resource_name]
		var current_quantity = resource_map.get(required_resource_name, 0)
		var projected_quantity = current_quantity - required_quantity
		if (projected_quantity < 0):
			return false
	return true

extends KinematicBody

const BlueprintResource = preload("res://src/interactable/building/blueprint/Blueprint.tscn")

onready var interaction_area = $InteractionArea
onready var attacking_area = $AttackingArea
onready var gun_position = $PlayerCore/GunPosition
onready var player_core = $PlayerCore
onready var blueprint_position = $PlayerCore/BlueprintPosition
var speed = 10
var max_life = 5
var current_life = 5
var movement = Vector3()
var inventory = preload("res://src/player/Inventory.gd").new()
var current_weapon = preload("res://src/weapon/Weapon.gd").new()
var is_sprinting = false
var waiting_for_interaction = false
var waiting_for_attack = false
var using_blueprint = null

func _ready():
	ServiceProvider.Player = self
	var weapon_model = preload("res://src/weapon/laser_gun/LaserGun.tscn").instance()
	current_weapon = weapon_model
	gun_position.add_child(weapon_model)

func _unhandled_key_input(event):
	if (event.is_action_pressed("dismiss")):
		if (using_blueprint):
			using_blueprint.queue_free()
			using_blueprint = null

func _physics_process(delta):
	if (movement):
		var rotation_offset = player_core.global_transform.origin + movement
		player_core.look_at(rotation_offset, Vector3.UP)
		move_and_collide(movement.normalized() * delta * speed)
	if (waiting_for_interaction): try_to_interact()
	if (waiting_for_attack): try_to_attack()
	movement.x = 0;
	movement.y = 0;
	movement.z = 0;

func _process(delta):
	movement.x = Input.get_axis("movement_left", "movement_right")
	movement.z = Input.get_axis("movement_up", "movement_down")
	waiting_for_interaction = Input.is_action_just_pressed("interaction")
	waiting_for_attack = Input.is_action_pressed("attack")
	is_sprinting = Input.is_action_just_pressed("sprint")

func try_to_interact():
	waiting_for_interaction = false
	ServiceProvider.Logging.info("Player trying to interact")
	if (using_blueprint):
		try_to_build()
		return
	var overlapping_bodies = interaction_area.get_overlapping_bodies()
	if (overlapping_bodies.empty()) : return
	var first_body : Node = overlapping_bodies[0]
	first_body.interact(self)

func try_to_attack():
	waiting_for_attack = false
	#var overlapping_bodies = attacking_area.get_overlapping_bodies()
	#if (overlapping_bodies.empty()) : return
	#var first_body : Node = overlapping_bodies[0]
	if (!current_weapon) : return
	# current_weapon.attack(self, first_body)
	current_weapon.attack(self)

func try_to_build():
	var success = using_blueprint.create(self)
	if (!success) : return
	using_blueprint.consume_resources()
	using_blueprint.queue_free()
	using_blueprint = null

func damage(value = 1):
	ServiceProvider.Logging.info("Player was damaged (%d)" % self.current_life)
	self.current_life -= value
	if (self.current_life <= 0):
		death()

func load_blueprint(building_type, source_inventory):
	var blueprint = BlueprintResource.instance()
	blueprint.building_target = building_type
	blueprint.source_inventory = source_inventory
	using_blueprint = blueprint
	blueprint_position.add_child(blueprint)

func death():
	ServiceProvider.Game.game_over()

extends Control

onready var button = $Button
onready var animation_player = $AnimationPlayer

func _ready():
	animation_player.play("default")

func _on_Button_pressed():
	get_tree().change_scene("res://src/scene/main_menu/MainMenu.tscn")

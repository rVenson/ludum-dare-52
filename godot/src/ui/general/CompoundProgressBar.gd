extends Control

onready var texture_rect = $TextureRect
onready var label = $Label
onready var texture_progress = $TextureProgress
onready var progress_label = $TextureProgress/ProgressLabel

func set_item(compound, value):
	label.text = str(compound.full_name)
	texture_progress.value = int(value)
	progress_label.text = str("%2d%%" % value)

extends Control

signal dismiss
onready var label = $VBoxContainer/HBoxContainer/Label
onready var timer = $Timer
onready var actor_name_label = $VBoxContainer/ActorName

func set_message(text, actor_name = "", texture = null):
	actor_name_label.text = actor_name
	label.text = text

func set_timer(wait_time = 10):
	timer.wait_time = wait_time
	timer.start()

func _on_Timer_timeout():
	queue_free()

func _on_Label_pressed():
	emit_signal("dismiss")

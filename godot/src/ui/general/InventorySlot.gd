extends Button

onready var label = $Label
var item = null

func set_slot(item):
	self.item = item
	update_ui()

func set_hint(text):
	hint_tooltip = str(text)

func set_enable(value):
	disabled = !value

func update_ui():
	if (item):
		label.text = item.full_name
		disabled = false
	else:
		label.text = ""
		disabled = true

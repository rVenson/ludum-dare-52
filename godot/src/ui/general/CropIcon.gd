extends Control

onready var texture_rect = $TextureRect

func set_icon(texture, text_hint, enabled = true):
	texture_rect.texture = texture
	hint_tooltip = text_hint
	texture_rect.modulate = Color.white if enabled else Color.black

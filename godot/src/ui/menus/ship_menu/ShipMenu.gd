extends Control

const FuelModel = preload("res://src/core/models/FuelModel.gd")

onready var inventory_panel = $HBoxContainer/VBoxContainer/InventoryPanel
onready var building_panel = $HBoxContainer/VBoxContainer/BuildingPanel
onready var ship_panel = $HBoxContainer/ShipPanel
var player = null
var ship = null

func reset():
	self.player = null
	self.ship = null
	inventory_panel.reset()
	ship_panel.reset()

func populate(player, ship):
	self.player = player
	self.ship = ship
	var player_inventory = player.inventory.get_all()
	var ship_inventory = ship.inventory.get_all()
	var ship_fuel = ship.fuel
	inventory_panel.populate(player_inventory)
	ship_panel.populate(ship_fuel, ship_inventory)
	# load building list
	var building_list = ServiceProvider.Game.unlocked_buildings
	building_panel.reset()
	building_panel.populate(building_list, self.ship.inventory)

func _on_ShipPanel_cancel():
	hide()

func _on_ShipPanel_confirm():
	hide()
	ServiceProvider.Game.win_game()

func _on_InventoryPanel_selected(item):
	var success = self.player.inventory.discard(item)
	if(!success) : return
	if (item is FuelModel):
		self.ship.add_fuel()
	else:
		self.ship.inventory.add(item)
	var player = self.player
	var ship = self.ship
	reset()
	populate(player, ship)

func _on_ShipPanel_selected(item):
	var success = self.ship.inventory.discard(item)
	if(!success) : return
	self.player.inventory.add(item)
	var player = self.player
	var ship = self.ship
	reset()
	populate(player, ship)

func _on_BuildingPanel_selected(item):
	self.player.load_blueprint(item, self.ship.inventory)
	hide()

extends Control

const CompoundModel = preload("res://src/core/models/CompoundModel.gd")

onready var inventory_panel = $HBoxContainer/InventoryPanel
onready var blender_panel = $HBoxContainer/BlenderPanel
var player = null
var blender = null

func reset():
	self.player = null
	self.blender = null
	inventory_panel.reset()
	blender_panel.reset()

func populate(player, blender):
	self.player = player
	self.blender = blender
	var player_inventory = player.inventory.get_all()
	var filtered_inventory = []
	for item in player_inventory:
		if (!item is CompoundModel):
			filtered_inventory.append(item)
	inventory_panel.populate(player_inventory, filtered_inventory)
	blender_panel.populate(blender)

func _on_InventoryPanel_selected(item):
	var success = self.blender.add_compound(item)
	var new_value = self.blender.compound_progress.get(item.name, 0)
	if (!success) : return
	blender_panel.update_compound(item, new_value)
	var player = self.player
	var blender = self.blender
	reset()
	populate(player, blender)

func _on_BlenderPanel_cancel():
	hide()

func _on_BlenderPanel_confirm():
	self.blender.blend_fuel()
	hide()

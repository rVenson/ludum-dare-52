extends Control

onready var inventory_panel = $HBoxContainer/InventoryPanel
onready var analyser_panel = $HBoxContainer/AnalyserPanel
var player = null
var analyser = null

func reset():
	self.player = null
	self.analyser = null
	inventory_panel.reset()
	analyser_panel.reset()

func populate(player, analyser, seeds_map : Dictionary, unlocked_seeds : Array):
	self.player = player
	self.analyser = analyser
	var player_inventory = player.inventory.get_all()
	var filtered_inventory = []
	for item in player_inventory:
		if (!seeds_map.has(item.name)):
			filtered_inventory.append(item)
	inventory_panel.populate(player_inventory, filtered_inventory)
	analyser_panel.populate(seeds_map, unlocked_seeds)

func _on_AnalyserPanel_cancel():
	hide()

func _on_AnalyserPanel_confirm(seed_selected):
	analyser.unlock_seed(seed_selected, player)
	hide()

func _on_InventoryPanel_selected(item):
	analyser_panel.seed_selected(item)

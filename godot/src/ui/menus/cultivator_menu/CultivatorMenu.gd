extends Control

const CropModel = preload("res://src/core/models/CropModel.gd")

onready var cultivator_panel = $HBoxContainer/CultivatorPanel
var player = null
var cultivator = null

func reset():
	self.player = null
	self.cultivator = null
	cultivator_panel.reset()

func populate(player, cultivator):
	var unlocked_seeds = ServiceProvider.Game.unlocked_seeds
	self.player = player
	self.cultivator = cultivator
	cultivator_panel.populate(unlocked_seeds)
	cultivator_panel.show_crop(cultivator.current_crop, cultivator.crop_evolution)

func _on_InventoryPanel_selected(item):
	cultivator_panel.crop_selected(item)

func _on_CultivatorPanel_cancel():
	hide()

func _on_CultivatorPanel_cultivate(crop_type):
	self.cultivator.cultivate(crop_type)
	hide()

func _on_CultivatorPanel_dump():
	self.cultivator.dump()
	hide()

func _on_CultivatorPanel_harvest():
	self.cultivator.harvest()
	hide()

extends Control

onready var inventory_panel = $HBoxContainer/InventoryPanel

func _ready():
	hide()

func _unhandled_key_input(event):
	if event.is_action_pressed("ui_cancel"):
		get_tree().set_input_as_handled()
		hide()

func reset():
	inventory_panel.reset()

func show():
	var player = ServiceProvider.Player
	var player_inventory = player.inventory.get_all()
	inventory_panel.populate(player_inventory)
	visible = true
	set_process_unhandled_key_input(true)

func hide():
	reset()
	visible = false
	set_process_unhandled_key_input(false)

func _on_ButtonResume_pressed():
	hide()

func _on_ButtonExit_pressed():
	get_tree().change_scene("res://src/scene/main_menu/MainMenu.tscn")

extends Control

const CropModel = preload("res://src/core/models/CropModel.gd")

onready var inventory_panel = $HBoxContainer/InventoryPanel
onready var synthesizer_panel = $HBoxContainer/SynthesizerPanel
var player = null
var synthesizer = null

func reset():
	self.player = null
	self.synthesizer = null
	inventory_panel.reset()
	synthesizer_panel.reset()

func populate(player, synthesizer):
	self.player = player
	self.synthesizer = synthesizer
	var player_inventory = player.inventory.get_all()
	var filtered_inventory = []
	for item in player_inventory:
		if (!item is CropModel):
			filtered_inventory.append(item)
	inventory_panel.populate(player_inventory, filtered_inventory)

func _on_InventoryPanel_selected(item):
	synthesizer_panel.crop_selected(item)

func _on_SynthesizerPanel_confirm(crop_item):
	synthesizer.synthesizer_crop(crop_item, self.player)
	hide()

func _on_SynthesizerPanel_cancel():
	hide()

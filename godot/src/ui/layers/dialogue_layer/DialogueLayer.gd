extends Control

const MessageBox = preload("res://src/ui/general/MessageBox.tscn")

onready var message_list = $MessageList
var default_timer = 10
var message_log = Array()

func add_message(text, actor = null):
	message_log.append({
		text: text,
		actor: actor,
	})
	var message_box = MessageBox.instance()
	message_list.add_child(message_box)
	message_box.set_message(text, actor.full_name, actor.texture)
	message_box.set_timer(default_timer)
	message_box.connect("dismiss", self, "_on_message_dismiss", [message_box])

func _on_message_dismiss(message_box):
	message_box.queue_free()

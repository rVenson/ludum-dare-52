extends Control

func _on_ButtonMainMenu_pressed():
	get_tree().paused = false
	get_tree().change_scene("res://src/scene/main_menu/MainMenu.tscn")

func _on_ButtonRestart_pressed():
	get_tree().paused = false
	get_tree().reload_current_scene()

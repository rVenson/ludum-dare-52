extends Control

onready var pause_menu = $PauseMenu
onready var analyser_menu = $AnalyserMenu
onready var synthetiser_menu = $SynthesizerMenu
onready var cultivator_menu = $CultivatorMenu
onready var ship_menu = $ShipMenu
onready var blender_menu = $BlenderMenu
onready var dialogue_layer = $DialogueLayer
onready var close_layer = $CloseLayer
onready var game_over_layer = $GameOverLayer

func pause_menu():
	get_tree().set_pause(true)
	pause_menu.show()
	yield(pause_menu, "hide")
	get_tree().set_pause(false)

func analyser_menu(analyser_instance, player):
	get_tree().set_pause(true)
	analyser_menu.reset()
	var seeds_map = ServiceProvider.GameDatabase.seeds
	var unlocked_seeds = ServiceProvider.Game.unlocked_seeds
	analyser_menu.populate(player, analyser_instance, seeds_map, unlocked_seeds)
	analyser_menu.show()
	yield(analyser_menu, "hide")
	get_tree().set_pause(false)

func synthesizer_menu(synthesizer_instance, player):
	get_tree().set_pause(true)
	synthetiser_menu.reset()
	var seeds_map = ServiceProvider.GameDatabase.seeds
	var unlocked_seeds = ServiceProvider.Game.unlocked_seeds
	synthetiser_menu.populate(player, synthesizer_instance)
	synthetiser_menu.show()
	yield(synthetiser_menu, "hide")
	get_tree().set_pause(false)

func cultivator_menu(cultivator_instance, player):
	get_tree().set_pause(true)
	cultivator_menu.reset()
	cultivator_menu.populate(player, cultivator_instance)
	cultivator_menu.show()
	yield(cultivator_menu, "hide")
	get_tree().set_pause(false)

func ship_menu(ship_instance, player):
	get_tree().set_pause(true)
	ship_menu.reset()
	ship_menu.populate(player, ship_instance)
	ship_menu.show()
	yield(ship_menu, "hide")
	get_tree().set_pause(false)

func blender_menu(blender_instance, player):
	get_tree().set_pause(true)
	blender_menu.reset()
	blender_menu.populate(player, blender_instance)
	blender_menu.show()
	yield(blender_menu, "hide")
	get_tree().set_pause(false)

func game_over():
	game_over_layer.show()

func show_close_layer():
	close_layer.start()

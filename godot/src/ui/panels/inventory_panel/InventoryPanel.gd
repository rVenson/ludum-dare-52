extends Control

signal selected(item)
onready var grid_container = $Panel/GridContainer

func reset():
	for slot in grid_container.get_children():
		slot.set_slot(null)
		if (slot.is_connected("pressed", self, "_on_slot_selected")):
			slot.disconnect("pressed", self, "_on_slot_selected")

func populate(items : Array, filtered_items : Array = Array()):
	var items_remaining = items.duplicate()
	for slot in grid_container.get_children():
		var item = items_remaining.pop_back()
		if (item):
			slot.set_slot(item)
			slot.set_enable(!filtered_items.has(item))
			slot.connect("pressed", self, "_on_slot_selected", [item])
		else:
			slot.set_slot(null)

func _on_slot_selected(item):
	emit_signal("selected", item)

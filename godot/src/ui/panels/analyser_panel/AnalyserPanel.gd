extends Control

const CropIconResource = preload("res://src/ui/general/CropIcon.tscn")

signal cancel
signal confirm
onready var progress_list = $Panel/VBoxContainer/ProgressList
onready var crop_description = $Panel/VBoxContainer/CropDetails/CropDescription
onready var crop_description_label = $Panel/VBoxContainer/CropDetails/CropDescription/Label
onready var crop_description_texture = $Panel/VBoxContainer/CropDetails/CropDescription/TextureRect
onready var empty_label = $Panel/VBoxContainer/CropDetails/EmptyLabel
onready var analyse_button = $Panel/VBoxContainer/ButtonList/AnalyseButton
var seed_selected = null

func reset():
	for child in progress_list.get_children():
		child.queue_free()
	empty_label.show()
	crop_description.hide()
	analyse_button.disabled = true

func populate(seeds_map : Dictionary, unlocked_seeds : Array):
	for seed_item in seeds_map.values():
		var icon = CropIconResource.instance()
		var texture = preload("res://icon.png")
		var enabled = unlocked_seeds.has(seed_item)
		progress_list.add_child(icon)
		icon.set_icon(texture, "Test", enabled)

func seed_selected(seed_item):
	var crop = seed_item.unlocks
	if (!crop) : return
	crop_description_label.text = crop.description
	empty_label.hide()
	crop_description.show()
	analyse_button.disabled = false
	self.seed_selected = seed_item

func _on_CancelButton_pressed():
	emit_signal("cancel")

func _on_AnalyseButton_pressed():
	emit_signal("confirm", self.seed_selected)

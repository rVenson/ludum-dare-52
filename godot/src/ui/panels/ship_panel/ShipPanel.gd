extends Control

const InventorySmallSlotResource = preload("res://src/ui/general/InventorySmallSlot.tscn")

signal cancel
signal confirm
signal selected
onready var grid_container = $Panel/HBoxContainer/Container/ScrollContainer/GridContainer
onready var progress_label = $Panel/HBoxContainer/Fuel/ProgressBar/ProgressLabel
onready var progress_bar = $Panel/HBoxContainer/Fuel/ProgressBar
onready var launch_button = $Panel/HBoxContainer/Container/ButtonList/LaunchButton

func reset():
	for slot in grid_container.get_children():
		slot.queue_free()
	launch_button.disabled = true

func populate(ship_fuel, items : Array, filtered_items : Array = Array()):
	var items_remaining = items.duplicate()
	for item in items:
		var slot = InventorySmallSlotResource.instance()
		grid_container.add_child(slot)
		slot.set_slot(item)
		slot.set_enable(!filtered_items.has(item))
		slot.connect("pressed", self, "_on_slot_selected", [item])
	progress_bar.value = ship_fuel
	progress_label.text = "%2d%%" % ship_fuel
	if (ship_fuel >= 100): launch_button.disabled = false

func _on_slot_selected(item):
	emit_signal("selected", item)

func _on_CancelButton_pressed():
	emit_signal("cancel")

func _on_LaunchButton_pressed():
	emit_signal("confirm")

extends Control

signal cancel
signal confirm
onready var compound_description = $Panel/VBoxContainer/CompoundDetails/CompoundDescription
onready var compound_description_label = $Panel/VBoxContainer/CompoundDetails/CompoundDescription/Label
onready var compound_description_texture = $Panel/VBoxContainer/CompoundDetails/CompoundDescription/TextureRect
onready var empty_label = $Panel/VBoxContainer/CompoundDetails/EmptyLabel
onready var synthesize_button = $Panel/VBoxContainer/ButtonList/SynthesizeButton
var crop_selected = null

func reset():
	empty_label.show()
	compound_description.hide()
	synthesize_button.disabled = true

func crop_selected(crop_item):
	compound_description_label.text = crop_item.description
	empty_label.hide()
	compound_description.show()
	synthesize_button.disabled = false
	self.crop_selected = crop_item

func _on_CancelButton_pressed():
	emit_signal("cancel")

func _on_SynthesizeButton_pressed():
	emit_signal("confirm", self.crop_selected)

extends Control

const CropButtonResource = preload("res://src/ui/general/CropButton.tscn")

signal cancel
signal harvest
signal dump
signal cultivate(crop_type)
onready var crop_description = $Panel/HBoxContainer/VBoxContainer/CropDetails/CropDescription
onready var crop_description_label = $Panel/HBoxContainer/VBoxContainer/CropDetails/CropDescription/Label
onready var crop_description_evolution = $Panel/HBoxContainer/VBoxContainer/CropDetails/CurrentEvolution
onready var crop_description_texture = $Panel/HBoxContainer/VBoxContainer/CropDetails/CropDescription/TextureRect
onready var empty_label = $Panel/HBoxContainer/VBoxContainer/CropDetails/EmptyLabel
onready var dump_button = $Panel/HBoxContainer/VBoxContainer/ButtonList/DumpButton
onready var cultivate_button = $Panel/HBoxContainer/VBoxContainer/ButtonList/CultivateButton
onready var harvest_button = $Panel/HBoxContainer/VBoxContainer/ButtonList/HarvestButton
onready var crop_selector = $Panel/HBoxContainer/CropSelector
onready var empty_crops_label = $Panel/HBoxContainer/EmptyLabel

var crop_selected = null

func reset():
	for button in crop_selector.get_children():
		button.queue_free()
	empty_crops_label.show()
	crop_selector.hide()
	empty_label.show()
	dump_button.hide()
	cultivate_button.hide()
	harvest_button.hide()
	crop_description_evolution.hide()
	crop_description.hide()

func populate(seed_list):
	for seed_item in seed_list:
		var crop_item = seed_item.unlocks
		if (!crop_item) : continue
		var crop_button = CropButtonResource.instance()
		crop_selector.add_child(crop_button)
		crop_button.text = crop_item.full_name
		crop_button.connect("pressed", self, "crop_selected", [crop_item])
	if (crop_selector.get_child_count() > 0):
		crop_selector.show()
		empty_crops_label.hide()

func show_crop(crop_item = null, crop_evolution = 0):
	if (!crop_item):
		cultivate_button.disabled = true
		cultivate_button.show()
		return
	crop_selector.hide()
	dump_button.show()
	dump_button.disabled = false
	harvest_button.disabled = true
	harvest_button.show()
	crop_selected(crop_item)
	crop_description_evolution.show()
	crop_description_evolution.text = "Crop Evolution: %2d" % crop_evolution
	if (crop_evolution >= 100.0):
		harvest_button.disabled = false
	
func crop_selected(crop_item):
	crop_description_label
	crop_description_label.text = crop_item.description
	empty_label.hide()
	crop_description.show()
	cultivate_button.disabled = false
	self.crop_selected = crop_item

func _on_CancelButton_pressed():
	emit_signal("cancel")

func _on_DumpButton_pressed():
	emit_signal("dump")

func _on_CultivateButton_pressed():
	emit_signal("cultivate", self.crop_selected)

func _on_HarvestButton_pressed():
	emit_signal("harvest")

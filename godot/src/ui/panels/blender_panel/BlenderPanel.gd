extends Control

const CompoundProgressBarResource = preload("res://src/ui/general/CompoundProgressBar.tscn")
const Blender = preload("res://src/interactable/building/fuel_blender/FuelBlender.gd")

signal cancel
signal confirm
onready var blender_button = $Panel/VBoxContainer/ButtonList/BlenderButton
onready var compound_list = $Panel/VBoxContainer/CompoundList

func reset():
	blender_button.disabled = true
	for child in compound_list.get_children():
		child.queue_free()

func populate(blender : Blender):
	var can_blender = true
	for compound_name in blender.compound_progress:
		var compound = ServiceProvider.GameDatabase.compounds.get(compound_name)
		if(!compound) : return
		var value = blender.compound_progress[compound_name]
		var progress_bar = CompoundProgressBarResource.instance()
		progress_bar.name = compound_name
		compound_list.add_child(progress_bar)
		progress_bar.set_item(compound, value)
		if (value < 100) : can_blender = false
	if (can_blender):
		blender_button.disabled = false

func update_compound(compound, value):
	var progress_bar = compound_list.get_node_or_null(compound.name)
	if (!progress_bar) : return
	progress_bar.set_item(compound, value)

func _on_CancelButton_pressed():
	emit_signal("cancel")

func _on_BlenderButton_pressed():
	emit_signal("confirm")

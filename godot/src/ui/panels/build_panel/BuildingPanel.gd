extends Control

const BuildingButtonResource = preload("res://src/ui/general/BuildingSlot.tscn")
const Inventory = preload("res://src/player/Inventory.gd")

signal selected(item)
onready var grid_container = $Panel/ScrollContainer/GridContainer

func reset():
	for slot in grid_container.get_children():
		slot.queue_free()

func populate(building_list : Array, ship_inventory : Inventory):
	for building in building_list:
		var slot = BuildingButtonResource.instance()
		grid_container.add_child(slot)
		slot.set_slot(building)
		var hint_text = building.required_resources
		slot.set_hint(hint_text)
		slot.connect("pressed", self, "_on_slot_selected", [building])
		var has_resources = ship_inventory.has_resources(building.required_resources)
		if (has_resources) : slot.disabled = false
		else: slot.disabled = true

func _on_slot_selected(item):
	emit_signal("selected", item)

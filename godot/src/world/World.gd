extends Spatial

const EnemyResource = preload("res://src/enemy/Enemy.tscn")

const RAID_COOLDOWN_TIME = 15
const MIN_RAID_PREP_TIME = 60
const VAR_RAID_PREP_TIME = 30
onready var projectiles = $Projectiles
onready var landed_ship = $Buildings/LandedShip
onready var enemies = $Enemies
onready var raid_enemies = $RaidEnemies
onready var raid_spawners = $RaidSpawners
onready var buildings = $Buildings
onready var raid_timer = $Triggers/RaidTimer
onready var raid_cooldown = $Triggers/RaidCooldown
onready var raid_trigger_area = $Triggers/RaidTriggerArea

func _ready():
	set_physics_process(false)

func _physics_process(delta):
	var enemy_count = raid_enemies.get_child_count()
	if (enemy_count < 1):
		ServiceProvider.Logging.info("No enemies found. Starting raid cooldown")
		ServiceProvider.Game.game_director.raid_finished()
		raid_cooldown.start(RAID_COOLDOWN_TIME)
		set_physics_process(false)

func start_first_raid():
	ServiceProvider.Game.game_director.enemy_alarm()
	spawn_enemy()
	yield(get_tree().create_timer(4, false), "timeout")
	spawn_enemy()
	yield(get_tree().create_timer(4, false), "timeout")
	spawn_enemy()
	set_physics_process(true)

func start_raid():
	var num_milestones = ServiceProvider.Game.game_director.milestones.size()
	var random_enemies = 1 + randi() % num_milestones
	for i in range(random_enemies):
		yield(get_tree().create_timer(4, false), "timeout")
		spawn_enemy()

func spawn_enemy():
	var enemy = EnemyResource.instance()
	var raid_spawner = raid_spawners.get_child(0)
	enemy.transform.origin = raid_spawner.transform.origin
	raid_enemies.add_child(enemy)
	var building_list = buildings.get_children()
	var raid_target = building_list[randi() % building_list.size()]
	enemy.set_raid_target(raid_target)
	ServiceProvider.Logging.info("Enemy spawned at %s. Moving to %s" % [
		enemy.transform.origin,
		raid_target.name
	])

func spawn_projectile(projectile : Spatial, origin : Vector3, direction : Vector3):
	projectiles.add_child(projectile)
	projectile.global_transform.origin = origin
	projectile.look_at(direction + origin, Vector3.UP)
	#var angle = transform.origin.angle_to(direction)
	# projectile.rotate(Vector3.UP, angle)
	# projectile.look_at(direction, Vector3.UP)

func spawn_building(building_type, position):
	var resource = building_type.game_resource
	if (!resource):
		ServiceProvider.Logging.info("Impossible to find the game resource for this blueprint")
		return
	var building = resource.instance()
	building.global_transform.origin = position
	buildings.add_child(building)
	ServiceProvider.Logging.info("%s was built" % building.name)

func _on_RaidTriggerArea_body_entered(body : Spatial):
	if (body.is_in_group('player')):
		raid_trigger_area.set_deferred("monitoring", false)
		start_first_raid()

func _on_RaidTimer_timeout():
	ServiceProvider.Logging.info("Starting Raid")
	start_raid()
	set_physics_process(true)

func _on_RaidCooldown_timeout():
	ServiceProvider.Logging.info("Raid cooldown finished. Preparing raid")
	var random_time = MIN_RAID_PREP_TIME + randi() % VAR_RAID_PREP_TIME
	raid_timer.start(random_time)

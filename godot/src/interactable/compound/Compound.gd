extends "res://src/interactable/Interactable.gd"

var CompoundModel = preload("res://src/core/models/CompoundModel.gd")

export var compound_type = "default_compound"

func interact(actor):
	ServiceProvider.Logging.info("%s interacted with %s" % [actor.name, self.name])
	var item_type = ServiceProvider.GameDatabase.compounds.get(compound_type, CompoundModel.new())
	var is_successful = actor.inventory.add(item_type)
	if (is_successful):
		queue_free()

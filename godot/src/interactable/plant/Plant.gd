extends "res://src/interactable/Interactable.gd"

var SeedModel = preload("res://src/core/models/SeedModel.gd")

export var seed_type = "default_seed"

func interact(actor):
	ServiceProvider.Logging.info("%s interacted with %s" % [actor.name, self.name])
	var item_type = ServiceProvider.GameDatabase.seeds.get(seed_type, SeedModel.new())
	var is_successful = actor.inventory.add(item_type)
	if (is_successful):
		queue_free()

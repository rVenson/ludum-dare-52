extends "res://src/interactable/Interactable.gd"

var CropModel = preload("res://src/core/models/CropModel.gd")

export var crop_type = "default_crop"

func interact(actor):
	ServiceProvider.Logging.info("%s interacted with %s" % [actor.name, self.name])
	var item_type = ServiceProvider.GameDatabase.crops.get(crop_type, CropModel.new())
	var is_successful = actor.inventory.add(item_type)
	if (is_successful):
		queue_free()

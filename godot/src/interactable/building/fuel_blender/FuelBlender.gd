extends "res://src/interactable/building/Building.gd"

const FuelModel = preload("res://src/core/models/FuelModel.gd")

var compound_progress = Dictionary()

func _ready():
	reset_compound_progress()

func reset_compound_progress():
	compound_progress = {
		"organic_compound": 0,
		"metallic_compound": 0,
		"gases_compound": 0,
		"thermal_compound": 0,
		"radioactive_compound": 0,
	}

func interact(actor):
	ServiceProvider.Logging.info("%s activated the %s" % [actor.name, self.name])
	ServiceProvider.UIManager.blender_menu(self, actor)

func add_compound(compound_type):
	if (!compound_progress.has(compound_type.name)):
		ServiceProvider.Logging.warn("Incompatible compound" % compound_type.name)
		return false
	var success = ServiceProvider.Player.inventory.discard(compound_type)
	if (!success):
		ServiceProvider.Logging.warn("Player doesn't have this compound" % compound_type.name)
		return false
	compound_progress[compound_type.name] += 20
	return true

func blend_fuel():
	for compound_name in compound_progress:
		var compound_value = compound_progress[compound_name]
		if (compound_value < 100):
			ServiceProvider.Logging.warn("Impossible to blender. Lack of %s" % compound_name)
			return false
	var success = ServiceProvider.Player.inventory.add(FuelModel.new())
	if (!success) : return false
	reset_compound_progress()
	ServiceProvider.Game.game_director.first_fuel_galoon()
	return true

extends "res://src/interactable/building/Building.gd"

func _ready():
	ServiceProvider.Game.game_director.analyser_built()

func interact(actor):
	ServiceProvider.Logging.info("%s activated the %s" % [actor.name, self.name])
	ServiceProvider.UIManager.analyser_menu(self, actor)

func unlock_seed(seed_item, player):
	var player_inventory = player.inventory
	var unlocked_seeds = ServiceProvider.Game.unlocked_seeds
	if (!seed_item.unlocks):
		ServiceProvider.Logging.warn("The seed %s has no crop to unlock" % seed_item.name)
		return false
	if (unlocked_seeds.has(seed_item)):
		ServiceProvider.Logging.warn("The seed %s has already been unlocked" % seed_item.name)
		return false
	var success = player_inventory.discard(seed_item)
	if (!success):
		ServiceProvider.Logging.warn("Player does not have any %s to discard" % seed_item.name)
		return false
	unlocked_seeds.append(seed_item)
	ServiceProvider.Game.game_director.first_seed_researched()
	if (seed_item.name == "seed_005"):
		ServiceProvider.Game.game_director.last_seed()
	return true

extends "res://src/interactable/building/Building.gd"

const CropModel = preload("res://src/core/models/CropModel.gd")
const PlayerController = preload("res://src/player/PlayerController.gd")

func interact(actor):
	ServiceProvider.Logging.info("%s activated the %s" % [actor.name, self.name])
	ServiceProvider.UIManager.synthesizer_menu(self, actor)

func synthesizer_crop(crop_type : CropModel, player : PlayerController):
	var player_inventory = player.inventory
	if (player_inventory.is_full(crop_type.compound_output, 1)):
		ServiceProvider.Logging.warn("The player's inventory is full to add this item quantity")
		return false
	var success = player_inventory.discard(crop_type)
	if (!success):
		ServiceProvider.Logging.warn("Player does not have any %s to discard" % crop_type.name)
		return false
	for compound_name in crop_type.compound_output:
		var compound = ServiceProvider.GameDatabase.compounds.get(compound_name)
		if (compound):
			var compound_total = crop_type.compound_output[compound_name]
			for i in range(compound_total):
				player_inventory.add(compound)
	return true

extends "res://src/interactable/building/Building.gd"

const Inventory = preload("res://src/player/Inventory.gd")

var inventory : Inventory = Inventory.new()
var fuel = 0
var has_warned = false

func _ready():
	inventory.set_store_capacity(1000)

func interact(actor):
	ServiceProvider.Logging.info("%s activated the %s" % [actor.name, self.name])
	ServiceProvider.UIManager.ship_menu(self, actor)

func add_fuel(value = 10):
	if (fuel >= 100):
		ServiceProvider.Logging.warn("The ship tank is full")
		return false
	fuel += value
	if (fuel > 100) : fuel = 100
	return true

func damage(value = 1):
	ServiceProvider.Logging.info("Landed ship was damaged (%d)" % self.current_life)
	self.current_life -= value
	if (!has_warned):
		ServiceProvider.Game.dialogue("Our ship! They\'re invading our ship!", "sarcastic_ai")
		has_warned = true
	if (self.current_life <= 0):
		ServiceProvider.Game.dialogue("OH NO!", "sarcastic_ai")
		death()

func death():
	ServiceProvider.Game.game_over()

func _on_WarningTimer_timeout():
	has_warned = false

extends Area

const BuildingModel = preload("res://src/core/models/BuildingsModel.gd")
const CompoundModel = preload("res://src/core/models/CompoundModel.gd")

var building_target : BuildingModel = null
var source_inventory = null

func create(player):
	if (!source_inventory) : return false
	if (!building_target) : return false
	var overlapping_bodies = get_overlapping_bodies()
	if (overlapping_bodies.size() > 0):
		ServiceProvider.Logging.info("Impossible to building this blueprint here")
		return false
	ServiceProvider.Game.world.spawn_building(building_target, global_transform.origin)
	return true

func consume_resources():
	var resources_map = building_target.required_resources
	for resource_name in resources_map:
		var compound = ServiceProvider.GameDatabase.compounds.get(resource_name)
		if (!compound) : return
		var quantity = resources_map[resource_name]
		for i in range(quantity):
			source_inventory.discard(compound)

extends "res://src/interactable/Interactable.gd"

export var max_life = 5
export var current_life = 5

func damage(value = 1):
	self.current_life -= value
	ServiceProvider.Logging.info("%s was damaged (%d)" % [self.name, self.current_life])
	if (self.current_life <= 0):
		death()

func death():
	queue_free()

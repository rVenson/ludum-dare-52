extends "res://src/interactable/building/Building.gd"

const LaserShootResource = preload("res://src/weapon/laser_gun/LaserShoot/LaserShoot.tscn")

onready var muzzle_position = $Gun/MuzzlePosition
onready var gun = $Gun
onready var base = $Base
var target : Spatial = null
var default_shoot_delay = 1.0
var current_shoot_delay = 1.0

func _ready():
	ServiceProvider.Game.game_director.first_defense()

func _physics_process(delta):
	current_shoot_delay -= delta
	if (target):
		aim_enemy()
		if (current_shoot_delay < 0):
			shoot()
		return
	random_rotation()

func shoot():
	var direction = -muzzle_position.global_transform.basis.z
	var origin = muzzle_position.global_transform.origin
	var shoot = LaserShootResource.instance()
	ServiceProvider.Game.world.spawn_projectile(shoot, origin, direction)
	current_shoot_delay = default_shoot_delay

func random_rotation():
	gun.rotate(Vector3.UP, 0.025)

func aim_enemy():
	var current_position = gun.global_transform.origin
	var target_position = target.global_transform.origin
	#var angle = current_position.angle_to(target_position)
	#gun.rotate(Vector3.UP, angle)
	gun.look_at(target_position, Vector3.UP)

func interact(actor):
	ServiceProvider.Logging.info("%s activated the %s" % [actor.name, self.name])

func _on_TargetArea_body_entered(body : Spatial):
	if (body.is_in_group("enemy")):
		target = body

func _on_TargetArea_body_exited(body : Spatial):
	if (body == target):
		target = null

extends "res://src/interactable/building/Building.gd"

var current_crop = null
var crop_evolution = 0

func _ready():
	set_physics_process(false)
	crop_evolution = 0
	ServiceProvider.Game.game_director.first_cultivation()

func _physics_process(delta):
	crop_evolution += (delta * 4)
	if (crop_evolution >= 100):
		crop_evolution = 100
		set_physics_process(false)

func interact(actor):
	ServiceProvider.Logging.info("%s activated the %s" % [actor.name, self.name])
	ServiceProvider.UIManager.cultivator_menu(self, actor)

func cultivate(crop_type):
	current_crop = crop_type
	update_mesh()
	set_physics_process(true)

func dump():
	current_crop = null
	crop_evolution = 0
	update_mesh()

func harvest():
	if (crop_evolution < 100) : return
	var success = ServiceProvider.Player.inventory.add(current_crop)
	if (!success) : return
	current_crop = null
	crop_evolution = 0
	update_mesh()
	ServiceProvider.Game.game_director.first_harvest()

func update_mesh():
	pass

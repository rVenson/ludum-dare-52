extends Node

func interact(actor):
	ServiceProvider.Logging.info("%s interacted with %s" % [actor.name, self.name])

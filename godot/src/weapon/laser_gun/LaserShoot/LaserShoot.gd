extends Area

var shoot_speed = 25
var shoot_duration = 1

func _physics_process(delta):
	shoot_duration -= delta
	var direction = transform.basis.z
	transform.origin -= delta * shoot_speed * direction
	if (shoot_duration < 0):
		ServiceProvider.Logging.info("Projectile freed at %s" % transform.origin)
		queue_free()

func _on_LaserShoot_body_entered(body : Node):
	if (body.is_in_group("target")):
		body.damage()
	queue_free()

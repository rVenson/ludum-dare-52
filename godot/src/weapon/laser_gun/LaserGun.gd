extends Node

onready var muzzle_position = $MuzzlePosition
var ammo = preload("res://src/weapon/laser_gun/LaserShoot/LaserShoot.tscn")
var default_use_delay = 0.2
var current_use_delay = 0.2

func _physics_process(delta):
	current_use_delay -= delta
	if (current_use_delay > 0) : return
	set_physics_process(false)

func attack(player : Node, target : Spatial = null):
	if (current_use_delay > 0) : return
	ServiceProvider.Logging.info("Firing laser gun")
	var shoot = ammo.instance()
	var origin = muzzle_position.global_transform.origin
	var direction = -muzzle_position.global_transform.basis.z
	ServiceProvider.Game.world.spawn_projectile(shoot, origin, direction)
	current_use_delay = default_use_delay
	set_physics_process(true)

extends Node

const LoggingClass = preload("res://src/core/logging/Logging.gd")
const GameDatabaseClass = preload("res://src/core/game_database/GameDatabase.gd")
const UIManagerClass = preload("res://src/ui/UIManager.gd")
const GameInstanceClass = preload("res://src/game/GameInstance.gd")
const PlayerController = preload("res://src/player/PlayerController.gd")
var Logging : LoggingClass = LoggingClass.new()
var GameDatabase : GameDatabaseClass = GameDatabaseClass.new()
var UIManager : UIManagerClass = null
var Game : GameInstanceClass = null
var Player : PlayerController = null

extends Reference

var name = "default_building"
var full_name = "Default Building"
var required_resources = Dictionary()
var game_resource : PackedScene

func _init(name : String, full_name : String, required_resources : Dictionary, game_resource : PackedScene):
	self.name = name
	self.full_name = full_name
	self.required_resources = required_resources
	self.game_resource = game_resource

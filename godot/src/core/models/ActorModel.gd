extends Reference

var name : String
var full_name : String
var texture : Texture

func _init(
		name : String = "default_actor",
		full_name : String = "Unknown",
		texture = preload("res://icon.png")
	):
	self.name = name
	self.full_name = full_name
	self.texture = texture

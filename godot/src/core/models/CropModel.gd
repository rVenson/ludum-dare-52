extends Reference

var name : String
var full_name : String
var description : String
var compound_output : Dictionary

func _init(
		name : String = "default_crop",
		full_name : String = "Default Crop",
		description : String = "",
		compound_output : Dictionary = Dictionary()
	):
	self.name = name
	self.full_name = full_name
	self.description = description
	self.compound_output = compound_output

extends Reference

const CropModel = preload("res://src/core/models/CropModel.gd")

var name : String
var full_name : String
var unlocks : CropModel

func _init(
		name : String = "default_seed",
		full_name : String = "Default Seed",
		unlocks : CropModel = null
	):
	self.name = name
	self.full_name = full_name
	self.unlocks = unlocks

extends Reference

var name = "default_compound"
var full_name = "Default Compound"

func _init(
		name : String = "default_compound",
		full_name : String = "Default Compound",
		unlocks : Array = Array()
	):
	self.name = name
	self.full_name = full_name

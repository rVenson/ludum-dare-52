extends Reference

var name : String
var full_name : String

func _init(
		name : String = "fuel",
		full_name : String = "Spaceship Fuel"
	):
	self.name = name
	self.full_name = full_name

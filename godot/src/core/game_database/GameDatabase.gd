extends Reference

const CompoundModel = preload("res://src/core/models/CompoundModel.gd")
const BuildingModel = preload("res://src/core/models/BuildingsModel.gd")
const CropModel = preload("res://src/core/models/CropModel.gd")
const SeedModel = preload("res://src/core/models/SeedModel.gd")
const ActorModel = preload("res://src/core/models/ActorModel.gd")

var compounds = Dictionary()
var buildings = Dictionary()
var crops = Dictionary()
var seeds = Dictionary()
var actors = Dictionary()

func _init():
	register_compounds()
	register_buildings()
	register_crops()
	register_seeds()
	register_actors()

func register_compounds():
	compounds.organic_compound = CompoundModel.new(
		"organic_compound",
		"Organic Compound"
	)
	compounds.metallic_compound = CompoundModel.new(
		"metallic_compound",
		"Metallic Compound"
	)
	compounds.gases_compound = CompoundModel.new(
		"gases_compound",
		"Gases Compound"
	)
	compounds.thermal_compound = CompoundModel.new(
		"thermal_compound",
		"Thermal Compound"
	)
	compounds.radioactive_compound = CompoundModel.new(
		"radioactive_compound",
		"Radioactive Compound"
	)

func register_buildings():
	buildings.biological_analyser = BuildingModel.new(
		"biological_analyser",
		"Biological Analyser",
		{
			"thermal_compound": 2,
			"metallic_compound": 2
		},
		preload("res://src/interactable/building/biological_analyser/BiologicalAnalyser.tscn")
	)
	buildings.cultivator = BuildingModel.new(
		"cultivator",
		"Cultivator",
		{
			"organic_compound": 2,
			"gases_compound": 1,
		},
		preload("res://src/interactable/building/cultivator/Cultivator.tscn")
	)
	buildings.resource_synthesizer = BuildingModel.new(
		"resource_synthesizer",
		"Resource Synthesizer",
		{
			"organic_compound": 2,
			"metallic_compound": 2
		},
		preload("res://src/interactable/building/resource_synthesizer/ResourceSynthesizer.tscn")
	)
	buildings.laser_gunner = BuildingModel.new(
		"laser_gunner",
		"Laser Turret",
		{
			"metallic_compound": 3,
			"thermal_compound": 3
		},
		preload("res://src/interactable/building/laser_gunner/LaserGunner.tscn")
	)
	buildings.fuel_blender = BuildingModel.new(
		"fuel_blender",
		"Fuel Blender",
		{
			"metallic_compound": 3,
			"gases_compound": 3,
			"thermal_compound": 3,
			"radioactive_compound": 3
		},
		preload("res://src/interactable/building/fuel_blender/FuelBlender.tscn")
	)

func register_crops():
	crops.plant_001 = CropModel.new(
		"plant_001",
		"Plant 001",
		"A flora species to gathering resources",
		{
			"organic_compound": 2,
		}
	)
	crops.plant_002 = CropModel.new(
		"plant_002",
		"Plant 002",
		"A flora species to gathering resources",
		{
			"metallic_compound": 1,
		}
	)
	crops.plant_003 = CropModel.new(
		"plant_003",
		"Plant 003",
		"A flora species to gathering resources",
		{
			"gases_compound": 2,
		}
	)
	crops.plant_004 = CropModel.new(
		"plant_004",
		"Plant 004",
		"A flora species to gathering resources",
		{
			"thermal_compound": 2,
		}
	)
	crops.plant_005 = CropModel.new(
		"plant_005",
		"Plant 005",
		"A flora species to gathering resources",
		{
			"radioactive_compound": 1,
		}
	)

func register_seeds():
	seeds.seed_001 = SeedModel.new(
		"seed_001",
		"Seed 001",
		crops.get('plant_001')
	)
	seeds.seed_002 = SeedModel.new(
		"seed_002",
		"Seed 002",
		crops.get('plant_002')
	)
	seeds.seed_003 = SeedModel.new(
		"seed_003",
		"Seed 003",
		crops.get('plant_003')
	)
	seeds.seed_004 = SeedModel.new(
		"seed_004",
		"Seed 004",
		crops.get('plant_004')
	)
	seeds.seed_005 = SeedModel.new(
		"seed_005",
		"Seed 005",
		crops.get('plant_005')
	)

func register_actors():
	actors.liekedeeler = ActorModel.new(
		"unknown",
		"Unknown Message"
	)
	actors.liekedeeler = ActorModel.new(
		"liekedeeler",
		"Cap. Liekedeeler"
	)
	actors.sarcastic_ai = ActorModel.new(
		"sarcastic_ai",
		"Sarcastic AI"
	)
	actors.time_capsule = ActorModel.new(
		"time_capsule",
		"Time Capsule"
	)

extends Object

func console_print(message):
	print(message)

func info(message):
	console_print(message)

func warn(message):
	console_print(message)

func error(message):
	console_print(message)

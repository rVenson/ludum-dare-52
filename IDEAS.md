# Ludum Dare 52
06 jan - 08 jan
Theme: Harvest

## Definitions of the Theme

Verb.
- to pick or collect plants or animals
- to take body parts from someone (medical purposes)
- to collect information (specially automatically)

## Random Ideias

- Futuristic / Sea ambient
- Story about desolated earth
- Player will be led to believe that the planet is an alien planet? (but the plot is that is the future earth)
- Player needs to escape from the planet
- Player needs to collect flora and analysing
- After analysing, combine the flora and create another species seeds
- Plant seeds in the base farm
- The last combination leads to the pandemic cure
- Robots and local fauna as enemies? Enemies will spawn in the world or raid the player base
- The difficulty of the raids is based on the player's progress (total flora researched progress)
- Plants as support to basic actions like shot, base defense, gas for vehicles, boosts for analysers, fertilizer
- Story: Robots are created to protect the flora but started attack the humans? They are wrongly programmed?
- Story: Pieces of the story spread across the world
- Every seed has some necessities of water, air composition, nutrition, temperature, light
- Objective: player's need a composition to escape from the planet. The ship run with biogas, but the composition will need to gathered from the local flora.
- Player inventory / capacity is limited, making the player needs to go back to the base (unlocked with the vehicle?)
- There's a day/night cycle, some plants only grows at some time period
- Story: you are a space pirate called Liekedeeler

### Games inspiration

- Subnautica
- Astroneer
- Factorio

## Theme Relations

- Harvest Plants
- Harvest information about the game

## Main Game Mechanics

- Base building
  - Gunners
  - Crop farm
  - Analyser Processor
- Resource System
  - Basic resources: Carbon, hydrogen, oxygen
  - Macronutrients = Nitrogen, phosphorus, Potassium, Sulphur
  - Micronutrients: calcium, magnesium, chlorine, manganese, boron, iron, zinc, cooper, nickel
- Evolve system
- Top-down shooter

## Gameloop

- Gather a single plant
- Analyse
- Plant
- Gather more
- Make resources (compounds)
- Renew the Analyser

## Gameplay

- Player starts next to the ship (with the gas counter at 0%)

- Gather some resources that are spread across the map
- The resources to make the initial buildings are next to the ship
- Build the Biological Analyser and the (after) Resource Synthesizer (you can only have one of each)
- Gather a plant next to the ship
- Puts on the analyser to get the plant stats
- The same plant now can be put on the Resource Synthesizer to get the resources and seeds
- The player can explore to get the first blueprint (the Cultivator) and start to create farms
- Far from the ship, there are enemies that the player can handle with the laser gun. The gun doesn't have unlimited bullets but can be recharged in the ship.
- The cultivator has some cost, so the player will need to gather some resources first
- In the cultivator, the player can grows the crops faster and without travel to collect and wait for time based crop growing (the plants will spawn very slowly)
- After some time, the plant grows and the player can repeat the process to get more resources
- This is the point where some enemies start to raid at night (if the player is far from the base, receives a warning from the ship, some warnings are send from the ship (actually some kind of sarcastic AI with some dialogues in the game))
- The enemies will attack the buildings in some order, with the ship being the last one, if the ship is destroyed, this is the game over (LOSS CONDITION)
- The resources can be used to create more buildings (Gunners, crop farm, analyser)
- The biological analyser needs to be revitalized after analyse a crop.
- The last unlockable blueprint will be the gas synthesizer, that will provide gas gallons when filled with the required resources
- After fill the gas gallons in the ship, the player can enter in the ship and exit the planet (VICTORY CONDITION)

## Game Entities

- Player
- Ship
- Buildings
  - Resource Synthesizer
  - Biological Analyser
  - Cultivator
  - Laser Gun
  - Fuel Blender
- Compounds (Resources)
  - Organic
  - Metallic
  - Gases
  - Thermal
  - Radioactive
  - Magnetic?
- Seeds
- Plants
  - Plant 1
  - Plant 2
  - Plant 3
  - Plant 4
  - Plant 5
- Enemies
  - Robot
- Companion
  - Sarcastic AI

## Time Planning

### Areas

Programming
Sound Effects
Soundtrack
2D Art
3D Art
Game Core

06 jan
17PM - Theme Announcement
19PM - Game ideas
10PM - Game loop ready
12PM - Core development
04AM - Sleep

07 jan
10AM - Wake up
12PM - 
08PM - Game core Completed
12AM - Game Balancing
04AM - Sleep

08 jan
15PM - Final Polishing
16PM - Bug Hunt
17PM - Final
18PM - Submission